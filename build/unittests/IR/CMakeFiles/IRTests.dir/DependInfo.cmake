# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/AttributesTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/AttributesTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/ConstantsTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/ConstantsTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/DominatorTreeTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/DominatorTreeTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/IRBuilderTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/IRBuilderTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/InstructionsTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/InstructionsTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/MDBuilderTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/MDBuilderTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/MetadataTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/MetadataTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/PassManagerTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/PassManagerTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/PatternMatch.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/PatternMatch.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/TypeBuilderTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/TypeBuilderTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/TypesTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/TypesTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/ValueMapTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/ValueMapTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/ValueTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/ValueTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/VerifierTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/VerifierTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR/WaymarkTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/unittests/IR/CMakeFiles/IRTests.dir/WaymarkTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "unittests/IR"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/unittests/IR"
  "include"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/include"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/utils/unittest/googletest/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/AsmParser/CMakeFiles/LLVMAsmParser.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Analysis/IPA/CMakeFiles/LLVMipa.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Analysis/CMakeFiles/LLVMAnalysis.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/IR/CMakeFiles/LLVMCore.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Object/CMakeFiles/LLVMObject.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/utils/unittest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/utils/unittest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
