# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/clang/unittests/Lex/LexerTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/clang/unittests/Lex/CMakeFiles/LexTests.dir/LexerTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/clang/unittests/Lex/PPCallbacksTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/clang/unittests/Lex/CMakeFiles/LexTests.dir/PPCallbacksTest.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/clang/unittests/Lex/PPConditionalDirectiveRecordTest.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/clang/unittests/Lex/CMakeFiles/LexTests.dir/PPConditionalDirectiveRecordTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CLANG_ENABLE_ARCMT"
  "CLANG_ENABLE_REWRITER"
  "CLANG_ENABLE_STATIC_ANALYZER"
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "tools/clang/unittests/Lex"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/clang/unittests/Lex"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/clang/include"
  "tools/clang/include"
  "include"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/include"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/utils/unittest/googletest/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/utils/unittest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/utils/unittest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/clang/lib/Lex/CMakeFiles/clangLex.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/clang/lib/Basic/CMakeFiles/clangBasic.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Object/CMakeFiles/LLVMObject.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
