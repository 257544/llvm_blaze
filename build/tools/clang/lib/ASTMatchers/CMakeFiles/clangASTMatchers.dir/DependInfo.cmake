# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/clang/lib/ASTMatchers/ASTMatchFinder.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/clang/lib/ASTMatchers/CMakeFiles/clangASTMatchers.dir/ASTMatchFinder.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/clang/lib/ASTMatchers/ASTMatchersInternal.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/clang/lib/ASTMatchers/CMakeFiles/clangASTMatchers.dir/ASTMatchersInternal.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CLANG_ENABLE_ARCMT"
  "CLANG_ENABLE_REWRITER"
  "CLANG_ENABLE_STATIC_ANALYZER"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "tools/clang/lib/ASTMatchers"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/clang/lib/ASTMatchers"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/clang/include"
  "tools/clang/include"
  "include"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/clang/lib/Basic/CMakeFiles/clangBasic.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/clang/lib/AST/CMakeFiles/clangAST.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/clang/lib/Lex/CMakeFiles/clangLex.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Object/CMakeFiles/LLVMObject.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
