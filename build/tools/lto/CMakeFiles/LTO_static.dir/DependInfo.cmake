# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/lto/LTOCodeGenerator.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/lto/CMakeFiles/LTO_static.dir/LTOCodeGenerator.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/lto/LTODisassembler.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/lto/CMakeFiles/LTO_static.dir/LTODisassembler.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/lto/LTOModule.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/lto/CMakeFiles/LTO_static.dir/LTOModule.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/lto/lto.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/tools/lto/CMakeFiles/LTO_static.dir/lto.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "LLVM_VERSION_INFO=\"3.3svn\""
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "tools/lto"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/tools/lto"
  "include"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
