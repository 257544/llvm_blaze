# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation/AddressSanitizer.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/AddressSanitizer.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation/BlackList.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/BlackList.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation/BoundsChecking.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/BoundsChecking.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation/EdgeProfiling.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/EdgeProfiling.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation/GCOVProfiling.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/GCOVProfiling.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation/Instrumentation.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/Instrumentation.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation/MemorySanitizer.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/MemorySanitizer.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation/OptimalEdgeProfiling.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/OptimalEdgeProfiling.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation/PathProfiling.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/PathProfiling.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation/ProfilingUtils.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/ProfilingUtils.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation/ThreadSanitizer.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/ThreadSanitizer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/Transforms/Instrumentation"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/Instrumentation"
  "include"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Analysis/CMakeFiles/LLVMAnalysis.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/IR/CMakeFiles/LLVMCore.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Utils/CMakeFiles/LLVMTransformUtils.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Analysis/IPA/CMakeFiles/LLVMipa.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Object/CMakeFiles/LLVMObject.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
