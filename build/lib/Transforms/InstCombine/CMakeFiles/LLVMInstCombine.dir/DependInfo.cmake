# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombineAddSub.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineAddSub.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombineAndOrXor.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineAndOrXor.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombineCalls.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineCalls.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombineCasts.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineCasts.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombineCompares.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineCompares.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombineLoadStoreAlloca.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineLoadStoreAlloca.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombineMulDivRem.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineMulDivRem.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombinePHI.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombinePHI.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombineSelect.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineSelect.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombineShifts.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineShifts.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombineSimplifyDemanded.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineSimplifyDemanded.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstCombineVectorOps.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstCombineVectorOps.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine/InstructionCombining.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/InstructionCombining.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/Transforms/InstCombine"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Transforms/InstCombine"
  "include"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Analysis/CMakeFiles/LLVMAnalysis.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/IR/CMakeFiles/LLVMCore.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Utils/CMakeFiles/LLVMTransformUtils.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Analysis/IPA/CMakeFiles/LLVMipa.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Object/CMakeFiles/LLVMObject.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
