# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/Mangler.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/Mangler.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/Target.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/Target.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/TargetIntrinsicInfo.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/TargetIntrinsicInfo.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/TargetJITInfo.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/TargetJITInfo.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/TargetLibraryInfo.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/TargetLibraryInfo.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/TargetLoweringObjectFile.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/TargetLoweringObjectFile.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/TargetMachine.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/TargetMachine.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/TargetMachineC.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/TargetMachineC.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/TargetSubtargetInfo.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/TargetSubtargetInfo.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/Target"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target"
  "include"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/IR/CMakeFiles/LLVMCore.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Object/CMakeFiles/LLVMObject.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
