# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeAsmPrinter.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeAsmPrinter.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeDelaySlotFiller.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeDelaySlotFiller.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeFrameLowering.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeFrameLowering.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeISelDAGToDAG.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeISelDAGToDAG.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeISelLowering.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeISelLowering.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeInstrInfo.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeInstrInfo.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeIntrinsicInfo.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeIntrinsicInfo.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeMCInstLower.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeMCInstLower.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeMachineFunction.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeMachineFunction.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeRegisterInfo.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeRegisterInfo.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeSelectionDAGInfo.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeSelectionDAGInfo.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeSubtarget.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeSubtarget.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeTargetMachine.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeTargetMachine.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze/MBlazeTargetObjectFile.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/CMakeFiles/LLVMMBlazeCodeGen.dir/MBlazeTargetObjectFile.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/Target/MBlaze"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/Target/MBlaze"
  "include"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/CodeGen/AsmPrinter/CMakeFiles/LLVMAsmPrinter.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/CodeGen/CMakeFiles/LLVMCodeGen.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/IR/CMakeFiles/LLVMCore.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/InstPrinter/CMakeFiles/LLVMMBlazeAsmPrinter.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/MCTargetDesc/CMakeFiles/LLVMMBlazeDesc.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/MBlaze/TargetInfo/CMakeFiles/LLVMMBlazeInfo.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/CodeGen/SelectionDAG/CMakeFiles/LLVMSelectionDAG.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Target/CMakeFiles/LLVMTarget.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/ObjCARC/CMakeFiles/LLVMObjCARCOpts.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Scalar/CMakeFiles/LLVMScalarOpts.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Transforms/Utils/CMakeFiles/LLVMTransformUtils.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Analysis/IPA/CMakeFiles/LLVMipa.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Analysis/CMakeFiles/LLVMAnalysis.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Object/CMakeFiles/LLVMObject.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
