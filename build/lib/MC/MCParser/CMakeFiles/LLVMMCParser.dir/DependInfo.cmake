# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/MC/MCParser/AsmLexer.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/AsmLexer.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/MC/MCParser/AsmParser.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/AsmParser.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/MC/MCParser/COFFAsmParser.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/COFFAsmParser.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/MC/MCParser/DarwinAsmParser.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/DarwinAsmParser.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/MC/MCParser/ELFAsmParser.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/ELFAsmParser.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/MC/MCParser/MCAsmLexer.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/MCAsmLexer.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/MC/MCParser/MCAsmParser.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/MCAsmParser.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/MC/MCParser/MCAsmParserExtension.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/MCAsmParserExtension.cpp.o"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/MC/MCParser/MCTargetAsmParser.cpp" "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/MCTargetAsmParser.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/MC/MCParser"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/lib/MC/MCParser"
  "include"
  "/home/maxbubblegum/Desktop/llvm_mblaze/llvm/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/home/maxbubblegum/Desktop/llvm_mblaze/build/lib/Object/CMakeFiles/LLVMObject.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
