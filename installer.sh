wget -nc http://llvm.org/releases/3.3/cfe-3.3.src.tar.gz
wget -nc http://llvm.org/releases/3.3/llvm-3.3.src.tar.gz

wget -nc http://llvm.org/releases/3.3/clang-tools-extra-3.3.src.tar.gz
rm -rf llvm
mkdir -p llvm
tar xfz llvm-3.3.src.tar.gz -C llvm --strip-components=1
mkdir -p llvm/tools/clang
tar xfz cfe-3.3.src.tar.gz -C llvm/tools/clang --strip-components=1
mkdir -p llvm/tools/clang/tools/extra
tar xfz clang-tools-extra-3.3.src.tar.gz -C llvm/tools/clang/tools/extra --strip-components=1

mkdir -p build
cd build
cmake ../llvm -DCMAKE_BUILD_TYPE=Release -DLLVM_TARGETS_TO_BUILD=MBlaze -DLLVM_ENABLE_PROJECTS="clang;openmp"
make -j$(nproc)